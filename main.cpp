#include <QGuiApplication>
#include <QQuickView>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setApplicationName("File Dialog Failure test case");

    QGuiApplication app(argc, argv);

    QQuickView view;
    view.setSource(QUrl("qrc:filedialogfail.qml"));
    view.show();

    return app.exec();
}
