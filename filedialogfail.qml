import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.13
import QtQuick.Dialogs 1.3

Rectangle {
    width: 500
    height: 500

    ColumnLayout {
        anchors.centerIn: parent

        Button {
            Layout.alignment: Qt.AlignHCenter
            text: qsTr("Show Save Dialog")
            onClicked: openDialog.open()
        }

        Label {
            text: "fileUrl: " + openDialog.fileUrl
        }
        Label {
            text: "fileUrls: " + openDialog.fileUrls
        }
        Label {
            text: "folder: " + openDialog.folder
        }
    }

    FileDialog {
        id: openDialog
        title: qsTr("This will fail in Flatpak")
        selectExisting: false // be a save dialog
        selectMultiple: false
        visible: false
    }
}
